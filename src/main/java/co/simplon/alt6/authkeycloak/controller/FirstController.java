package co.simplon.alt6.authkeycloak.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
public class FirstController {

    @GetMapping("/api/public")
    public String pub() {
        return "Coucou";
    }
    

    @GetMapping("/api/protected")
    public String prot(@AuthenticationPrincipal Jwt jwt) {
        System.out.println(jwt.getSubject());
        return "secret";
    }
    
}
