package co.simplon.alt6.authkeycloak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthkeycloakApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthkeycloakApplication.class, args);
	}

}
